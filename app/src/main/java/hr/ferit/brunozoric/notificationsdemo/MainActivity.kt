package hr.ferit.brunozoric.notificationsdemo

import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createNotificationChannels()
        likeAction.setOnClickListener{displayLikeNotification() }
        commentAction.setOnClickListener{displayCommentNotification() }
    }

    private fun displayLikeNotification() {

        val intent = Intent(this, MainActivity::class.java)
        // We can put extra in the intent.

        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )

        val notification = NotificationCompat.Builder(this, getChannelId(CHANNEL_LIKES))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Like!")
            .setContentText("Liked your post.")
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .build()

        NotificationManagerCompat.from(this)
            .notify(1001, notification)
    }

    private fun displayCommentNotification() {
        // Homework!
    }
}
